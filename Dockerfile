FROM alpine:3.12

ARG TARGETARCH
ARG TERRAFORM_VERSION=0.13.0

LABEL com.hashicorp.terraform.version=${TERRAFORM_VERSION}

RUN apk add --no-cache curl jq && \
  if [ "$TARGETARCH" = "arm64" ]; then TARGETARCH=arm; fi && \
  mkdir -p "/tmp/terraform.install" && cd "/tmp/terraform.install" && \
  curl -fsSL -O "https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_${TARGETARCH}.zip" && \
  unzip "terraform_${TERRAFORM_VERSION}_linux_${TARGETARCH}.zip" && \
  cp terraform /bin/terraform && \
  chmod u=rwX,go=rX /bin/terraform && \
  cd / && rm -rf "/tmp/terraform.install"

COPY gitlab-terraform.sh /usr/local/bin/gitlab-terraform
RUN chmod +x /usr/local/bin/gitlab-terraform
